package com.utn.ejemplo.contenedores

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

// Esta Activity no hace mas que mostrar el layout del ejemplo de ConstraintLayout
class ConstraintLayoutActivity: AppCompatActivity(), View.OnClickListener {
    private var layout = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constraint_layout)

        // hay que llamar setContentView(), eso se hace en proximoLayout()
        proximoLayout()
    }

    override fun onClick(v: View?) {
        proximoLayout()
    }

    // Determina el proximo layout que necesita mostrarse, y lo muestra.
    // El resultado es que a medida que el usuario hace click en cada pantalla, el layout cambia
    // de manera cíclica
    private fun proximoLayout() {
        val proximoLayout = when(layout) {
            R.layout.activity_constraint_layout -> R.layout.activity_constraint_layout_2
            else -> R.layout.activity_constraint_layout
        }
        layout = proximoLayout
        setContentView(layout)
        val raiz = findViewById<View>(R.id.raiz)

        // Esta clase implementa View.OnClickListener, así que no necesitamos crear un object como hicimos en otros ejemplos.
        raiz.setOnClickListener(this)
    }

}
