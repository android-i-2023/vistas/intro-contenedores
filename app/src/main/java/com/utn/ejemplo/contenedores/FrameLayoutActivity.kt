package com.utn.ejemplo.contenedores

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class FrameLayoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame_layout)

        // Cualquier lugar donde se haga click cambia a otro XML para ver otro ejemplo
        val raiz = findViewById<View>(R.id.raiz)
        raiz.setOnClickListener {
            setContentView(R.layout.activity_frame_layout_2)
        }

    }
}
