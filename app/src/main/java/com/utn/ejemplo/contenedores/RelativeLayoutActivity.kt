package com.utn.ejemplo.contenedores

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

// Esta Activity no hace mas que mostrar el layout del ejemplo de RelativeLayout
class RelativeLayoutActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_layout)
    }
}
