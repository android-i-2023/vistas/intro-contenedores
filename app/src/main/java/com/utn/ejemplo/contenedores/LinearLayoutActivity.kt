package com.utn.ejemplo.contenedores

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

/**
 * Esta Activity muestra ciclicamente los 3 ejemplos de LinearLayout, a medida que el usuario hace click
 * Para eso tiene una propiedad privada layout, que guarda el numero de layout que se debe mostrar.
 *
 * Si la pantalla es rotada, se vuelve al primer ejemplo, vale decir no se guarda el valor correcto
 * entre rotaciones de la pantalla, porque no vimos eso todavía.
 */
class LinearLayoutActivity: AppCompatActivity(), View.OnClickListener{

    private var layout = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // hay que llamar setContentView(), eso se hace en proximoLayout()
        proximoLayout()
    }

    override fun onClick(v: View?) {
        proximoLayout()
    }

    // Determina el proximo layout que necesita mostrarse, y lo muestra.
    // El resultado es que a medida que el usuario hace click en cada pantalla, el layout cambia
    // de manera cíclica
    private fun proximoLayout() {
        val proximoLayout = when(layout) {
            0 -> R.layout.activity_linear_layout_1
            R.layout.activity_linear_layout_1 -> R.layout.activity_linear_layout_2
            R.layout.activity_linear_layout_2 -> R.layout.activity_linear_layout_3
            else -> R.layout.activity_linear_layout_1
        }
        layout = proximoLayout
        setContentView(layout)
        val raiz = findViewById<View>(R.id.raiz)

        // Esta clase implementa View.OnClickListener, así que no necesitamos crear un object como hicimos en otros ejemplos.
        raiz.setOnClickListener(this)
    }
}
