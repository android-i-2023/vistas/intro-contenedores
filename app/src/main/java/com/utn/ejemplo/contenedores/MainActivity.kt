package com.utn.ejemplo.contenedores

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlin.reflect.KClass

// Presenta el menú de ejemplos de contenedores
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnFrameLayout: Button = findViewById(R.id.btn_frame_layout)
        btnFrameLayout.setOnClickListener {
            iniciarActivity(FrameLayoutActivity::class.java)
        }
        val btnLinearLayout: Button = findViewById(R.id.btn_linear_layout)
        btnLinearLayout.setOnClickListener {
            iniciarActivity(LinearLayoutActivity::class.java)
        }
        val btnRelativeLayout: Button = findViewById(R.id.btn_relative_layout)
        btnRelativeLayout.setOnClickListener {
            iniciarActivity(RelativeLayoutActivity::class.java)
        }
        val btnConstraintLayout: Button = findViewById(R.id.btn_constraint_layout)
        btnConstraintLayout.setOnClickListener {
            iniciarActivity(ConstraintLayoutActivity::class.java)
        }
    }

    // Inicia una nueva Activity de la clase indicada
    // Esto está aća porque necesitamos poder ver varios ejemplos en varias pantallas,
    // no se preocupen por no entender esto ahora.
    private fun iniciarActivity(clase: Class<out Activity>) {
        val intent = Intent(this, clase)
        startActivity(intent)
    }
}